﻿using EjemploWinFormEF.Marcas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploWinFormEF
{
    public partial class MenuForm : Form
    {
        private int childFormNumber = 0;

        public MenuForm()
        {
            InitializeComponent();
        }             

        private void marcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MarcasForm f = new MarcasForm();
            f.MdiParent = this;
            f.Show();
        }

    }
}
