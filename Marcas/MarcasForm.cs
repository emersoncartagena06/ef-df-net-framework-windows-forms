﻿using EjemploWinFormEF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploWinFormEF.Marcas
{
    public partial class MarcasForm : Form
    {

        private bool estaGuardando = false;

        public MarcasForm()
        {
            InitializeComponent();
        }

        private void MarcasForm_Load(object sender, EventArgs e)
        {
            listarMarcas();
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView1.CurrentRow != null && estaGuardando == false)
            {
                using (autosEntities db = new autosEntities())
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;
                    marca model = new marca();

                    model.id_marca = row.Cells["ColIdMarca"].Value == DBNull.Value ? 0 : int.Parse(row.Cells["ColIdMarca"].Value.ToString());
                    model.marca1 = row.Cells["ColMarca"].Value == DBNull.Value ? "" : row.Cells["ColMarca"].Value.ToString();
                    model.pais = row.Cells["ColPais"].Value == DBNull.Value ? "" : row.Cells["ColPais"].Value.ToString();

                    if(model.id_marca == 0)
                    {
                        db.marca.Add(model);
                    }
                    else
                    {
                        db.Entry<marca>(model).State = EntityState.Modified;
                    }

                    estaGuardando = true;
                    db.SaveChanges();
                    row.Cells["ColIdMarca"].Value = model.id_marca;
                    estaGuardando = false;
                }
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (dataGridView1.CurrentRow != null )
            {
                using (autosEntities db = new autosEntities())
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;
                    int idMarca = int.Parse(row.Cells["ColIdMarca"].Value.ToString());
                    marca model = db.marca.Where(x => x.id_marca == idMarca).First();
                    if(model != null)
                    {
                        db.Entry<marca>(model).State = EntityState.Deleted;
                        db.SaveChanges();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }

        }

        public void listarMarcas()
        {
            dataGridView1.AutoGenerateColumns = false;
            autosEntities db = new autosEntities();
            DataTable dt = ToDataTable(db.marca.ToList());
            dataGridView1.DataSource = dt;
            db.Dispose();
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
                
    }
}
